# Desafio Front-end: Abertura de Empresas
Olá, queremos te desafiar a participar do nosso Time Vox. Podemos começar? Seu trabalho será visto pelo nosso time técnico e você receberá ao final um feedback sobre o seu desafio. Interessante?

## O desafio
Construir um sistema de solicitações de abertura de empresa. Veja o mockup e use como base para as telas. 
![mockup](/assets/mockup.png)

A questão prioritária é o bom funcionamento de cada elemento, mas também quanto mais próximo a apresentação do layout do que foi proposto, será um diferencial.

### Tela inicial
- Listar as solicitações de abertura de empresa, podendo visualizar os principais dados de uma dada empresa, caso clique em 'Visualizar' e caso clique em 'Editar' redirecionar para a página específica.
- Deve-se ainda tem opção para cadastro de solicitações clicando em 'Solicitar Abertura'.

### Tela de Adição/Edição de solicitações
- Nova página com formulário dos dados pessoais, endereço e dados de empresa para preenchimento ou já preenchidas caso seja edição.
- Ao finalizar o preenchimento e estando válido o formulário ao clicar em 'Salvar' deve-se:
  - Salvar a solicitação com o endpoint post da api fake, citada mais abaixo;
  - Exibir modal com mensagem de sucesso;
  - e retornar para a tela inicial listando as solicitações salvas.

### Requisitos do projeto
 - Use o framework Angular na versão LTS ou até três versões anteriores à LTS
 - Use o gerenciador de pacotes npm
 - Crie um repositório privado no Gitlab
 - Submissão das requisições com API faker, segue documentação:

| Descrição do endpoint           | Método Http | Endpoint                                                                        |
| ------------------------------- | ----------- | ------------------------------------------------------------------------------- |
| Solicitações de abertura salvas | GET         | http://localhost:3000/empresas                                                  |
| Visualizar empresa              | GET         | http://localhost:3000/empresas/:id                                              |
| Salvar solicitação de abertura  | POST        | http://localhost:3000/empresas                                                  |
| Atualizar Empresa               | PUT         | http://localhost:3000/empresas/:id                                              |
| Opções de Entidade de registro  | GET         | http://localhost:3000/entidade-registro                                         |
| Lista de Estados                | GET         | https://servicodados.ibge.gov.br/api/v1/localidades/estados/                    |

### Recomendações
 - Bootstrap CSS e ngx-bootstrap
 - Pode utilizar alguma biblioteca extra, só evite o exagero, pois será prioritário a análise da implementação do uso do angular, rxjs e typescript.
 - Usar o json-server como api-faker (Não precisa criar um projeto backend para a demanda)
  - Instale ele localmente e coloque o arquivo [db.json](mocks/db.json) que está neste repositório numa pasta `mocks` do seu projeto. 
  - No package.json coloque em scripts:

```json
"scripts": {
  "mock": "npx json-server --watch mocks/db.json",
}
```

### O que vamos avaliar
- Estrutura do código
- Parte visual bem estruturada
- Integração com a API
- Commits com poucas alterações, sendo bem coerente e semântico nas mensagens

## Avaliaremos como um diferencial:
- Boas práticas de codificação e bom uso dos recursos do angular
- Experiência do usuário, principalmente validações e feedbacks visuais
- Documentar a estrutura do projeto, principais funções, arquivos e comandos de execução

### Como nos enviar a resolução
- Crie um repositório privado na Github.com;
- Desenvolva. Você terá 4 (quatro) dias a partir da data do envio do desafio;
- Após concluir seu trabalho, atualize o Github e adicione como membro (develop) o usuário `natancardosodev` 
- Crie um arquivo README.md com a explicação de como devemos executar o projeto e com uma descrição do que foi feito, o que não fez e/ou o que poderia melhorar; 
- Pronto! Agora é so esperar o nosso feedback... Boa sorte!!
